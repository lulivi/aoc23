#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest

from solution import parse_engine_schematic, parse_engine_schematic_gear_ratio


class TestParseEngineSchematic(unittest.TestCase):
    def test_if_schematic_with_number_just_above_of_symbol_is_properly_parsed(self) -> None:
        # Arrange
        input_schematic = [
            "..123.....",
            "...=......",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_on_left_upper_diagonal_of_symbol_is_properly_parsed(
        self,
    ) -> None:
        # Arrange
        input_schematic = [
            "..123.....",
            ".....=....",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_on_right_upper_diagonal_of_symbol_is_properly_parsed(
        self,
    ) -> None:
        # Arrange
        input_schematic = [
            "...123....",
            "..=.......",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_on_the_right_of_the_symbol_is_properly_parsed(self) -> None:
        # Arrange
        input_schematic = [
            "..=123....",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_on_the_left_of_the_symbol_is_properly_parsed(self) -> None:
        # Arrange
        input_schematic = [
            "..123=....",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_just_below_of_symbol_is_properly_parsed(self) -> None:
        # Arrange
        input_schematic = [
            "...=......",
            "..123.....",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_on_left_lower_diagonal_of_symbol_is_properly_parsed(
        self,
    ) -> None:
        # Arrange
        input_schematic = [
            ".....=....",
            "..123.....",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_if_schematic_with_number_on_right_lower_diagonal_of_symbol_is_properly_parsed(
        self,
    ) -> None:
        # Arrange
        input_schematic = [
            "..=.......",
            "...123....",
        ]
        expected_output = 123

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_schematic_with_simple_input(self) -> None:
        # Arrange
        input_schematic = [
            "467..114..",
            "...*......",
            "..35%.633.",
            "......#...",
            "617*......",
            "..*..+.58.",
            "..592.....",
            "......755.",
            "...$.*....",
            ".664.598..",
        ]
        expected_output = 5605

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_schematic_with_more_difficult_input(self) -> None:
        # Arrange
        input_schematic = [
            "......644............612.......254..638..............802.................................118.....................................317.691....",
            ".....*......321..176....+........&...=...906........*.......=518................994..938.*.....579....35....155...........320...........$...",
            "...939.@225........*......................$........41...............1......./.....+......102....*.....*.....412.......603....*.412=1........",
        ]
        expected_output = 9007

        # Act
        obtained_output = parse_engine_schematic(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)


class TestParseEngineSchematicGearRatio(unittest.TestCase):
    def test_it_should_find_no_gears(self) -> None:
        # Arrange
        input_schematic = [
            "..123.....",
            "...*..3...",
        ]
        expected_output = 0

        # Act
        obtained_output = parse_engine_schematic_gear_ratio(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_return_the_gear_ratio_properly(self) -> None:
        # Arrange
        input_schematic = [
            "..123.....",
            ".....*22..",
        ]
        expected_output = 2706

        # Act
        obtained_output = parse_engine_schematic_gear_ratio(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_return_the_gear_ratio_with_example_input(self) -> None:
        # Arrange
        input_schematic = [
            "467..114..",
            "...*......",
            "..35..633.",
            "......#...",
            "617*......",
            ".....+.58.",
            "..592.....",
            "......755.",
            "...$.*....",
            ".664.598..",
        ]
        expected_output = 467835

        # Act
        obtained_output = parse_engine_schematic_gear_ratio(input_schematic)

        # Assert
        self.assertEqual(obtained_output, expected_output)


if __name__ == "__main__":
    unittest.main(verbosity=3)
