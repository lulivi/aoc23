#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import re
import string

from pathlib import Path
from typing import List

SOLUTION_DIR = Path(__file__).resolve().parent
INPUT_FILE = SOLUTION_DIR.joinpath("input")


def parse_engine_schematic(engine_schematic: List[str]) -> int:
    engine_schematic_len = len(engine_schematic)
    part_numbers = []

    for index in range(len(engine_schematic)):
        for symbol_position in _find_symbol_positions_in_line(engine_schematic[index]):
            if index > 0:
                search_engine_schematic_index = index - 1
                line_part_numbers = _find_adjacent_numbers_to_symbols_in_chunk(
                    symbol_index=symbol_position,
                    engine_schematic_line=engine_schematic[search_engine_schematic_index],
                )
                part_numbers.extend(line_part_numbers)

            line_part_numbers = _find_adjacent_numbers_to_symbols_in_chunk(
                symbol_index=symbol_position,
                engine_schematic_line=engine_schematic[index],
            )
            part_numbers.extend(line_part_numbers)

            if index < engine_schematic_len - 1:
                search_engine_schematic_index = index + 1
                line_part_numbers = _find_adjacent_numbers_to_symbols_in_chunk(
                    symbol_index=symbol_position,
                    engine_schematic_line=engine_schematic[search_engine_schematic_index],
                )
                part_numbers.extend(line_part_numbers)

    return sum(map(int, part_numbers))


def _find_symbol_positions_in_line(schematic_line: str) -> List[int]:
    return [
        index
        for index, char in enumerate(schematic_line)
        if char in set(string.punctuation.replace(".", ""))
    ]


def _find_adjacent_numbers_to_symbols_in_chunk(
    symbol_index: int, engine_schematic_line: str
) -> List[str]:
    if engine_schematic_line[symbol_index].isdecimal():
        left_part = _find_number(chunk=engine_schematic_line[symbol_index::-1])[::-1]
        right_part = _find_number(chunk=engine_schematic_line[symbol_index:])
        return [f"{left_part}{right_part[1:]}"]

    numbers: List[str] = []
    if engine_schematic_line[symbol_index - 1].isdecimal():
        numbers.append(_find_number(chunk=engine_schematic_line[symbol_index - 1 :: -1])[::-1])
    if engine_schematic_line[symbol_index + 1].isdecimal():
        numbers.append(_find_number(chunk=engine_schematic_line[symbol_index + 1 :]))

    return numbers


def _find_number(chunk: str) -> str:
    if not chunk or not chunk[0].isdecimal():
        return ""

    number = ""
    for index, char in enumerate(chunk):
        if not chunk[index].isdecimal():
            return number

        number = f"{number}{char}"

    return number


def parse_engine_schematic_gear_ratio(engine_schematic: List[str]) -> int:
    engine_schematic_len = len(engine_schematic)
    part_numbers: List[int] = []

    for index in range(len(engine_schematic)):
        for symbol_position in _find_asterisk_positions_in_line(engine_schematic[index]):
            gear_part_numbers = []
            if index > 0:
                gear_part_numbers.extend(
                    _find_adjacent_numbers_to_symbols_in_chunk(
                        symbol_index=symbol_position,
                        engine_schematic_line=engine_schematic[index - 1],
                    )
                )

            gear_part_numbers.extend(
                _find_adjacent_numbers_to_symbols_in_chunk(
                    symbol_index=symbol_position,
                    engine_schematic_line=engine_schematic[index],
                )
            )

            if index < engine_schematic_len - 1:
                gear_part_numbers.extend(
                    _find_adjacent_numbers_to_symbols_in_chunk(
                        symbol_index=symbol_position,
                        engine_schematic_line=engine_schematic[index + 1],
                    )
                )

            if len(gear_part_numbers) == 2:
                part_numbers.append(int(gear_part_numbers[0]) * int(gear_part_numbers[1]))

    return sum(part_numbers)


def _find_asterisk_positions_in_line(schematic_line: str) -> List[int]:
    positions = []
    position = schematic_line.find("*")

    while position != -1:
        positions.append(position)
        position = schematic_line.find("*", position + 1)

    return positions


def parse_args(solutions: List[str]) -> argparse.Namespace:
    if not solutions:
        exit("There are no solutions for the day")

    parser = argparse.ArgumentParser()
    parser.add_argument("--part", choices=solutions, default=solutions[0])
    parser.add_argument("--list-solutions", action="store_true")
    args = parser.parse_args()

    if args.list_solutions:
        print(*solutions)
        exit()

    return args


def main():
    solutions = {
        "one": parse_engine_schematic,
        "two": parse_engine_schematic_gear_ratio,
    }
    args = parse_args(list(solutions.keys()))

    sum = solutions[args.part](INPUT_FILE.read_text().splitlines())

    print(f"The solution for part {args.part} is:", sum)


if __name__ == "__main__":
    main()
