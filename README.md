# Solutions 2023's Advent of Code.

## Usage

In order to run the solutions and the tests you will need to install Poetry:

```sh
pip3 install poetry==1.4.1
```

Once installed, you can install the dependencies:

```sh
poetry install
```

And start the virtualenv created by poetry:

```sh
poetry shell
```

Now you can prompt the solutions and tests:

```sh
nox -e solution
nox -e test
```

If you want to show only the solution or test for an specific day you can run:

```sh
nox -e solution -- 3
nox -e test -- 3
```

## License

You can find the license in the [LICENSE](./LICENSE) file.
