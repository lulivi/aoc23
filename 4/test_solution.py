#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import textwrap
import unittest

from solution import (
    calculate_sum_of_points_from_winning_cards,
    calculate_total_number_of_scratchcards,
)


class TestCalculateWinningCardsPoints(unittest.TestCase):
    def test_it_should_get_no_points_from_a_loosing_game(self) -> None:
        # Arrange
        game_input = textwrap.dedent(
            """Card 1: 41 48 83 86 17 |  1  2  3  4  5  6  7  8
            Card 1: 24 99 80 35 19 |  9 10 11 12 17 13 14 15"""
        )
        expected_output = 0

        # Act
        obtained_output = calculate_sum_of_points_from_winning_cards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_get_4_points_for_3_winning_numbers_in_a_card(self) -> None:
        # Arrange
        game_input = "Card 1: 41 48 83 86 17 | 41 86  6 31  1  9 48 53"
        expected_output = 4

        # Act
        obtained_output = calculate_sum_of_points_from_winning_cards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_get_8_points_for_3_winning_numbers_with_one_repeating_in_a_card(
        self,
    ) -> None:
        # Arrange
        game_input = "Card 1: 41 48 83 86 17 | 41 86  6 48  1  9 48 53"
        expected_output = 8

        # Act
        obtained_output = calculate_sum_of_points_from_winning_cards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_get_13_points_from_the_example_pile_of_scratchcards(self) -> None:
        # Arrange
        game_input = textwrap.dedent(
            """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
            Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
            Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
            Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
            Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
            Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""
        )
        expected_output = 13

        # Act
        obtained_output = calculate_sum_of_points_from_winning_cards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)


class TestCalculateTotalNumberOfScratchcards(unittest.TestCase):
    def test_it_should_obtain_no_extras_scratchcards(self) -> None:
        # Arrange
        game_input = textwrap.dedent(
            """Card 1: 41 48 83 86 17 |  1  2  3  4  5  6  7  8
            Card 1: 24 99 80 35 19 |  9 10 11 12 17 13 14 15"""
        )
        expected_output = 2

        # Act
        obtained_output = calculate_total_number_of_scratchcards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_obtain_one_copy_of_the_second_scratchcard_making_a_total_of_three(
        self,
    ) -> None:
        # Arrange
        game_input = textwrap.dedent(
            """Card 1: 41 48 83 86 17 |  41  2  3  4  5  6  7  8
            Card 2: 24 99 80 35 19 |  9 10 11 12 17 13 14 15"""
        )
        expected_output = 3

        # Act
        obtained_output = calculate_total_number_of_scratchcards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_it_should_obtain_a_total_of_thirty_scratchcards_from_six_originals(self) -> None:
        # Arrange
        game_input = textwrap.dedent(
            """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
            Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
            Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
            Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
            Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
            Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""
        )
        expected_output = 30

        # Act
        obtained_output = calculate_total_number_of_scratchcards(game_input)

        # Assert
        self.assertEqual(obtained_output, expected_output)


if __name__ == "__main__":
    unittest.main(verbosity=3)
