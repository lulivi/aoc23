#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import functools
import re
import string

from pathlib import Path
from typing import Dict, List

SOLUTION_DIR = Path(__file__).resolve().parent
INPUT_FILE = SOLUTION_DIR.joinpath("input")


def calculate_sum_of_points_from_winning_cards(pile_of_colorfull_cards: str) -> int:
    points = 0

    for card in filter(None, pile_of_colorfull_cards.splitlines()):
        points += _calculate_card_points(_get_how_many_numbers_won_in_card(card))

    return points


def _calculate_card_points(winning_numbers_count: int, total_points: int = 0) -> int:
    if winning_numbers_count < 2:
        return winning_numbers_count

    return 2 * _calculate_card_points(winning_numbers_count - 1, total_points)


def _get_how_many_numbers_won_in_card(card: str) -> int:
    _, winning_numbers_str, obtained_numbers_str = tuple(
        map(lambda part: part.strip(), card.replace(":", "|").split("|"))
    )

    winning_numbers = tuple(map(lambda number: number.strip(), winning_numbers_str.split()))

    return len(
        [
            got_number
            for got_number in map(lambda number: number.strip(), obtained_numbers_str.split())
            if got_number in winning_numbers
        ]
    )


def calculate_total_number_of_scratchcards(pile_of_colorfull_cards: str) -> int:
    cards_list = list(filter(None, pile_of_colorfull_cards.splitlines()))
    number_of_cards = len(cards_list)
    card_counts = [1] * number_of_cards

    for card_index, card_contents in enumerate(cards_list):
        winning_numbers_count = _get_how_many_numbers_won_in_card(card=card_contents)
        _duplicate_following_cards(
            card_index=card_index,
            cards_to_duplicate=winning_numbers_count,
            card_counts=card_counts,
        )

    return sum(card_counts)


def _duplicate_following_cards(
    card_index: int, cards_to_duplicate: int, card_counts: List[int]
) -> None:
    for relative_card_index in range(1, cards_to_duplicate + 1):
        card_counts[card_index + relative_card_index] += card_counts[card_index]


def parse_args(solutions: List[str]) -> argparse.Namespace:
    if not solutions:
        exit("There are no solutions for the day")

    parser = argparse.ArgumentParser()
    parser.add_argument("--part", choices=solutions, default=solutions[0])
    parser.add_argument("--list-solutions", action="store_true")
    args = parser.parse_args()

    if args.list_solutions:
        print(*solutions)
        exit()

    return args


def main():
    solutions = {
        "one": calculate_sum_of_points_from_winning_cards,
        "two": calculate_total_number_of_scratchcards,
    }
    args = parse_args(list(solutions.keys()))

    sum = solutions[args.part](INPUT_FILE.read_text())

    print(f"The solution for part {args.part} is:", sum)


if __name__ == "__main__":
    main()
