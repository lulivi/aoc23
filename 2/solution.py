#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import functools
import re

from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import DefaultDict, Dict, List, Optional, Tuple

from typing_extensions import Final, final

SOLUTION_DIR = Path(__file__).resolve().parent
INPUT_FILE = SOLUTION_DIR.joinpath("input")


GameHand = Dict[str, int]
GameID = int


@final
@dataclass
class BagContents:
    red: int = 0
    green: int = 0
    blue: int = 0

    def is_valid(self, game_hand: GameHand) -> bool:
        if (
            self.red >= game_hand.get("red", 0)
            and self.green >= game_hand.get("green", 0)
            and self.blue >= game_hand.get("blue", 0)
        ):
            return True

        return False


def get_id_if_game_is_valid(game_information: str, bag_contents: BagContents) -> int:
    """Return the ID of the game only if it was possible."""

    game_id, game_hands = _get_game_hands(game_information)

    if all(map(bag_contents.is_valid, game_hands)):
        return game_id

    return 0


def _get_game_hands(game_information: str) -> Tuple[GameID, List[GameHand]]:
    game_id_str, raw_hands = game_information.split(":")
    game_id = int(game_id_str.split()[-1])

    found_hands: List[GameHand] = [
        {
            ball[1].strip(): int(ball[0].strip())
            for ball in [raw_ball.split() for raw_ball in raw_hand.split(",")]
        }
        for raw_hand in raw_hands.split(";")
    ]

    return game_id, found_hands


def get_power_of_minimum_set_of_cubes_for_a_valid_game(game_information: str, *_, **__) -> int:
    colors = _get_list_of_hands_by_color(game_information)

    return functools.reduce(
        lambda x, y: x * y, [max(ball_number) for ball_number in colors.values()]
    )


def _get_list_of_hands_by_color(game_information: str) -> Dict[str, List[int]]:
    raw_game_hands = game_information.split(":")[-1]
    color_appearances = defaultdict(lambda: [])

    balls = [ball.strip().split(" ") for ball in raw_game_hands.replace(";", ",").split(",")]

    for number, name in balls:
        color_appearances[name].append(int(number))

    return color_appearances


def get_power_of_minimum_set_of_cubes_for_a_valid_game_with_regex(
    game_information: str, *_, **__
) -> int:
    raw_game_hands = game_information.split(":")[-1]

    return functools.reduce(
        lambda x, y: x * y,
        [
            max(map(int, re.findall(rf"(\d+) {color}", raw_game_hands)))
            for color in ("red", "green", "blue")
        ],
    )


def parse_args(solutions: List[str]) -> argparse.Namespace:
    if not solutions:
        exit("There are no solutions for the day")

    parser = argparse.ArgumentParser()
    parser.add_argument("--part", choices=solutions, default=solutions[0])
    parser.add_argument("--list-solutions", action="store_true")
    args = parser.parse_args()

    if args.list_solutions:
        print(*solutions)
        exit()

    return args


def main():
    solutions = {
        "one": get_id_if_game_is_valid,
        "two": get_power_of_minimum_set_of_cubes_for_a_valid_game,
        "two_with_regex": get_power_of_minimum_set_of_cubes_for_a_valid_game_with_regex,
    }
    args = parse_args(list(solutions.keys()))

    sum = 0
    bag_contents: Final = BagContents(red=12, green=13, blue=14)

    for line in INPUT_FILE.read_text().splitlines():
        sum += solutions[args.part](
            game_information=line,
            bag_contents=bag_contents,
        )

    print(f"The solution for part {args.part} is:", sum)


if __name__ == "__main__":
    main()
