#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest

from solution import (
    BagContents,
    get_id_if_game_is_valid,
    get_power_of_minimum_set_of_cubes_for_a_valid_game,
    get_power_of_minimum_set_of_cubes_for_a_valid_game_with_regex,
)


class TestBagContents(unittest.TestCase):
    def test_provided_game_info_is_valid(self) -> None:
        # Arrange
        bag_contents = BagContents(
            red=3,
            green=2,
            blue=9,
        )
        provided_game_info = {
            "red": 3,
            "green": 2,
            "blue": 9,
        }

        # Act
        is_valid = bag_contents.is_valid(provided_game_info)

        # Assert
        self.assertTrue(is_valid)

    def test_provided_game_info_is_not_valid(self) -> None:
        # Arrange
        bag_contents = BagContents(
            red=3,
            green=2,
            blue=9,
        )
        provided_game_info = {
            "red": 30,
            "green": 32,
            "blue": 123,
        }

        # Act
        is_valid = bag_contents.is_valid(provided_game_info)

        # Assert
        self.assertFalse(is_valid)


class TestGetIdIfGameIsValid(unittest.TestCase):
    def test_game_validator_when_input_line_is_valid(self) -> None:
        # Arrange
        input_line = "Game 5: 1 red, 2 blue; 1 blue, 3 red, 2 green"
        expected_output = 5

        # Act
        obtained_output = get_id_if_game_is_valid(
            game_information=input_line, bag_contents=BagContents(red=3, green=2, blue=5)
        )

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_game_validator_when_input_is_not_valid(self) -> None:
        # Arrange
        input_line = "Game 5: 10 red, 2 blue"
        expected_output = 0

        # Acta
        obtained_output = get_id_if_game_is_valid(
            game_information=input_line, bag_contents=BagContents(red=3, green=2, blue=5)
        )

        # Assert
        self.assertEqual(obtained_output, expected_output)


class testGetPowerOfMinimumSetOfCubesForAValidGameWithRegex(unittest.TestCase):
    def test_the_power_obtained_is_correct(self) -> None:
        # Arrange
        input_line = "Game 5: 1 red, 2 blue; 1 blue, 3 red, 2 green"
        expected_output = 12

        # Act
        obtained_output = get_power_of_minimum_set_of_cubes_for_a_valid_game(
            game_information=input_line
        )

        # Assert
        self.assertEqual(obtained_output, expected_output)


class TestGetPowerOfMinimumSetOfCubesForAValidGame(unittest.TestCase):
    def test_the_power_obtained_is_correct(self) -> None:
        # Arrange
        input_line = "Game 5: 1 red, 2 blue; 1 blue, 3 red, 2 green"
        expected_output = 12

        # Act
        obtained_output = get_power_of_minimum_set_of_cubes_for_a_valid_game_with_regex(
            game_information=input_line
        )

        # Assert
        self.assertEqual(obtained_output, expected_output)


if __name__ == "__main__":
    unittest.main(verbosity=3)
