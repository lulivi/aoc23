#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import re

from pathlib import Path
from typing import List

SOLUTION_DIR = Path(__file__).resolve().parent
INPUT_FILE = SOLUTION_DIR.joinpath("input")


def extract_number_from_calibration_line(calibration_line: str) -> int:
    digits = _extract_digits_from_calibration_line(calibration_line)
    return _form_number_from_digit_list(digits)


def _extract_digits_from_calibration_line(calibration_line: str) -> List[str]:
    return [char for char in calibration_line if char.isnumeric()]


def _form_number_from_digit_list(digit_list: List[str]) -> int:
    if not digit_list:
        return 0

    return int(f"{digit_list[0]}{digit_list[-1]}")


def extract_number_from_calibration_line_with_spelled_digits(
    calibration_line: str,
) -> int:
    word_to_digit = {
        "one": "1",
        "two": "2",
        "three": "3",
        "four": "4",
        "five": "5",
        "six": "6",
        "seven": "7",
        "eight": "8",
        "nine": "9",
    }
    base_regex = r"(\d|one|two|three|four|five|six|seven|eight|nine)"
    first_number = _get_match(
        rf"{base_regex}",
        calibration_line,
    )
    first_digit = word_to_digit.get(first_number, first_number)

    if not first_digit:
        return 0

    last_number = _get_match(
        rf".*{base_regex}",
        calibration_line,
    )
    last_digit = word_to_digit.get(last_number, last_number)

    return int(f"{first_digit}{last_digit}")


def _get_match(pattern: str, string: str) -> str:
    match = re.search(pattern, string)

    if match is None:
        return ""

    return match.group(1)


def parse_args(solutions: List[str]) -> argparse.Namespace:
    if not solutions:
        exit("There are no solutions for the day")

    parser = argparse.ArgumentParser()
    parser.add_argument("--part", choices=solutions, default=solutions[0])
    parser.add_argument("--list-solutions", action="store_true")
    args = parser.parse_args()

    if args.list_solutions:
        print(*solutions)
        exit()

    return args


def main():
    solutions = {
        "one": extract_number_from_calibration_line,
        "two": extract_number_from_calibration_line_with_spelled_digits,
    }
    args = parse_args(list(solutions.keys()))
    sum = 0

    for line in INPUT_FILE.read_text().splitlines():
        sum += solutions[args.part](line)

    print(f"The solution for part {args.part} is:", sum)


if __name__ == "__main__":
    main()
