#!/usr/bin/env python3
# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest

from solution import (
    extract_number_from_calibration_line,
    extract_number_from_calibration_line_with_spelled_digits,
)


class TestExtraction(unittest.TestCase):
    def test_number_is_properly_extracted_from_calibration_line(self) -> None:
        # Arrange
        input_line = "fivek5mfzrdxfbn66nine8eight"
        expected_output = 58

        # Act
        obtained_output = extract_number_from_calibration_line(input_line)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_repeated_digit_is_extracted_from_calibration_line(self) -> None:
        # Arrange
        input_line = "dgkclm3seven"
        expected_output = 33

        # Acta
        obtained_output = extract_number_from_calibration_line(input_line)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_no_number_is_extracted_from_calibration_line(self) -> None:
        # Arrange
        input_line = "dgkclmseven"
        expected_output = 0

        # Acta
        obtained_output = extract_number_from_calibration_line(input_line)

        # Assert
        self.assertEqual(obtained_output, expected_output)


class TestExtractionWithDigitNames(unittest.TestCase):
    def test_number_is_properly_extracted_from_calibration_line(self) -> None:
        # Arrange
        input_line = "fivek2mfzrdxfbn66nine1eight"
        expected_output = 58

        # Act
        obtained_output = extract_number_from_calibration_line_with_spelled_digits(input_line)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_repeated_digit_is_extracted_from_calibration_line(self) -> None:
        # Arrange
        input_line = "dgkclm3ffjksa"
        expected_output = 33

        # Acta
        obtained_output = extract_number_from_calibration_line_with_spelled_digits(input_line)

        # Assert
        self.assertEqual(obtained_output, expected_output)

    def test_no_number_is_extracted_from_calibration_line(self) -> None:
        # Arrange
        input_line = "dgkclmsffevien"
        expected_output = 0

        # Acta
        obtained_output = extract_number_from_calibration_line_with_spelled_digits(input_line)

        # Assert
        self.assertEqual(obtained_output, expected_output)


if __name__ == "__main__":
    unittest.main(verbosity=3)
