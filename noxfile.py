# Copyright (c) 2023 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import sys

from pathlib import Path
from typing import List, Optional, cast

import nox

ROOT_DIRECTORY = Path(__file__).parent.resolve()
nox.options.default_venv_backend = "venv"
nox.options.reuse_existing_virtualenvs = True
nox.options.sessions = ["test", "lint"]
nox.options.stop_on_first_error = False
nox.options.error_on_missing_interpreters = False


def get_present_solution_directories(only: Optional[int] = None) -> List[Path]:
    def solution_dir_from_day(day_number: int) -> Path:
        return ROOT_DIRECTORY.joinpath(str(day_number))

    if only is not None and only > 0 and only < 25:
        return [solution_dir_from_day(only)]

    return [day_path for day_path in map(solution_dir_from_day, range(1, 25)) if day_path.is_dir()]


@nox.session(venv_backend="none")
def solution(session: nox.Session) -> None:
    python_executable = Path(sys.executable).resolve()
    logging.getLogger("nox").setLevel(logging.WARNING)

    only: Optional[int] = None

    if len(session.posargs) == 1 and session.posargs[0].isnumeric():
        only = int(session.posargs[0])

    for solution_dir in get_present_solution_directories(only=only):
        with session.cd(solution_dir):
            session.warn(f"Solutions for day {solution_dir.name}:")
            solutions_str = cast(
                str,
                session.run(
                    str(python_executable), "solution.py", "--list-solutions", silent=True
                ),
            )
            for solution in solutions_str.split():
                session.run(str(python_executable), "solution.py", "--part", solution)


@nox.session
def lint(session: nox.Session) -> None:
    """Lint the code."""
    session.run("poetry", "install", "--with", "lint", external=True)

    session.run("black", "--check", "--diff", str(ROOT_DIRECTORY))
    session.run("isort", "--check", "--diff", str(ROOT_DIRECTORY))
    session.run("mypy", str(ROOT_DIRECTORY))


@nox.session(python=["3.8"])
def test(session: nox.Session) -> None:
    session.run("poetry", "install", "--with", "test", external=True)
    logging.getLogger("nox").setLevel(logging.WARNING)

    only: Optional[int] = None

    if len(session.posargs) == 1 and session.posargs[0].isnumeric():
        only = int(session.posargs[0])

    for solution_dir in get_present_solution_directories(only=only):
        with session.cd(solution_dir):
            session.warn(f"Tests for day {solution_dir.name}:")
            session.run(
                "pytest",
                "-vvv",
                "--emoji",
                str(solution_dir),
            )


@nox.session(venv_backend="none")
def format(session: nox.Session) -> None:
    """Format the code."""
    session.run("black", str(ROOT_DIRECTORY))
    session.run("isort", str(ROOT_DIRECTORY))
